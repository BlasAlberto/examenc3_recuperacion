var cantidadColumnas = 0;
var edad = 0;
var altura = 0.0;
var peso = 0.0;
var IMC = 0.0;
var nivel = "";

function generarDatos(){
    let inputEdad = document.getElementById('inputEdad');
    let inputAltura = document.getElementById('inputAltura');
    let inputPeso = document.getElementById('inputPeso');
    let inputIMC = document.getElementById('inputIMC');
    let inputNivel = document.getElementById('inputNivel');

    edad = Math.floor(Math.random()*81)+18;
    altura = ((Math.random()*1)+1.5).toFixed(1);
    peso = Math.floor(Math.random()*110)+20;

    inputEdad.value = edad;
    inputAltura.value = altura;
    inputPeso.value = peso;
    inputIMC.value = "";
    inputNivel.value = "";
}

function calcularIMC(){
    let inputIMC = document.getElementById('inputIMC');
    let inputNivel = document.getElementById('inputNivel');

    IMC = (peso/(altura*altura)).toFixed(1);

    if(IMC < 18.5)
        nivel = "Bajo peso";
    else if(IMC >= 18.5  &&  IMC < 25)
        nivel = "Peso saludable";
    else if(IMC >= 25  &&  IMC < 30)
        nivel = "Sobre peso";
    else if(IMC >= 30)
        nivel = "Obesidad";

    inputIMC.value = IMC;
    inputNivel.value = nivel;
}

function registrarValores(){
    let inputEdad = document.getElementById('inputEdad');
    let inputAltura = document.getElementById('inputAltura');
    let inputPeso = document.getElementById('inputPeso');
    let inputIMC = document.getElementById('inputIMC');
    let inputNivel = document.getElementById('inputNivel');
    let divRegistros = document.getElementById('divRegistros');
    let imcPromedio = document.getElementById('imcPromedio');

    cantidadColumnas++;
    let tabla = divRegistros.innerHTML;
    tabla +=
        "<div class='columna'>" +
            "<p>" + cantidadColumnas +  "</p> <hr>" +
            "<p>" + edad +  "</p> <hr>" +
            "<p>" + altura +  "</p> <hr>" +
            "<p>" + peso +  "</p> <hr>" +
            "<p id='imc" + cantidadColumnas + "'>" + IMC +  "</p> <hr>" +
            "<p>" + nivel +  "</p> <hr>";
    divRegistros.innerHTML = tabla;

    let promedio = 0.0;
    for(let i=0; i<cantidadColumnas; i++){
        let divRegistros = document.getElementById('imc'+(i+1));
        promedio += parseFloat(divRegistros.innerHTML);
    } 
    imcPromedio.innerHTML = "IMC Promedio: " + promedio.toFixed(1);

    inputEdad.value = ""; edad = "";
    inputAltura.value = ""; altura = "";
    inputPeso.value = ""; peso = "";
    inputIMC.value = ""; IMC = "";
    inputNivel.value = ""; nivel = "";
}

function borrarRegistro(){
    let inputEdad = document.getElementById('inputEdad');
    let inputAltura = document.getElementById('inputAltura');
    let inputPeso = document.getElementById('inputPeso');
    let inputIMC = document.getElementById('inputIMC');
    let inputNivel = document.getElementById('inputNivel');
    let divRegistros = document.getElementById('divRegistros');
    let imcPromedio = document.getElementById('imcPromedio');

    divRegistros.innerHTML = 
        "<center><h3>Registros</h3></center>" + 
        "<div id='divColumnas' class='columna'>" +
            "<p>Num.</p> <hr>" +
            "<p>Edad</p> <hr>" +
            "<p>Altura</p> <hr>" +
            "<p>Peso</p> <hr>" +
            "<p>IMC</p> <hr>" +
            "<p>Nivel</p> </div>";
    
    cantidadColumnas = 0;
    imcPromedio.innerHTML = "IMC Promedio: 0";
    inputEdad.value = edad;
    inputAltura.value = altura;
    inputPeso.value = peso;
    inputIMC.value = "";
    inputNivel.value = "";
}